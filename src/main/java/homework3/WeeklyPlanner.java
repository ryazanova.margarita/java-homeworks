package homework3;

import java.util.Arrays;
import java.util.Scanner;

public class WeeklyPlanner {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String[][] schedule = createAPlanner();
        System.out.print("Please, input the day of the week: ");

        while (true) {
            String day = scan.nextLine().toLowerCase().trim();
            String plan;
            String[][] schedule2 = Arrays.copyOf(schedule, schedule.length);
            switch (day) {
                case "monday" -> System.out.println("Your tasks for " + schedule[0][0] + ": " + schedule[0][1]);
                case "tuesday" -> System.out.println("Your tasks for " + schedule[1][0] + ": " + schedule[1][1]);
                case "wednesday" -> System.out.println("Your tasks for " + schedule[2][0] + ": " + schedule[2][1]);
                case "thursday" -> System.out.println("Your tasks for " + schedule[3][0] + ": " + schedule[3][1]);
                case "friday" -> System.out.println("Your tasks for " + schedule[4][0] + ": " + schedule[4][1]);
                case "saturday" -> System.out.println("Your tasks for " + schedule[5][0] + ": " + schedule[5][1]);
                case "sunday" -> System.out.println("Your tasks for " + schedule[6][0] + ": " + schedule[6][1]);
                case "exit" -> System.exit(0);
                case "change monday", "reschedule monday" -> {
                    System.out.print("Input a new task for " + schedule[0][0] + ": ");
                    plan = scan.nextLine();
                    schedule2[0][1] = plan;
                    schedule[0][1] = schedule2[0][1];
                    System.out.println("Great!");
                }
                case "change tuesday", "reschedule tuesday" -> {
                    System.out.print("Input a new task for " + schedule[1][0] + ": ");
                    plan = scan.nextLine();
                    schedule2[1][1] = plan;
                    schedule[1][1] = schedule2[1][1];
                    System.out.println("Great!");
                }
                case "change wednesday", "reschedule wednesday" -> {
                    System.out.print("Input a new task for " + schedule[2][0] + ": ");
                    plan = scan.nextLine();
                    schedule2[2][1] = plan;
                    schedule[2][1] = schedule2[2][1];
                    System.out.println("Great!");
                }
                case "change thursday", "reschedule thursday" -> {
                    System.out.print("Input a new task for " + schedule[3][0] + ": ");
                    plan = scan.nextLine();
                    schedule2[3][1] = plan;
                    schedule[3][1] = schedule2[3][1];
                    System.out.println("Great!");
                }
                case "change friday", "reschedule friday" -> {
                    System.out.print("Input a new task for " + schedule[4][0] + ": ");
                    plan = scan.nextLine();
                    schedule2[4][1] = plan;
                    schedule[4][1] = schedule2[4][1];
                    System.out.println("Great!");
                }
                case "change saturday", "reschedule saturday" -> {
                    System.out.print("Input a new task for " + schedule[5][0] + ": ");
                    plan = scan.nextLine();
                    schedule2[5][1] = plan;
                    schedule[5][1] = schedule2[5][1];
                    System.out.println("Great!");
                }
                case "change sunday", "reschedule sunday" -> {
                    System.out.print("Input a new task for " + schedule[6][0] + ": ");
                    plan = scan.nextLine();
                    schedule2[6][1] = plan;
                    schedule[6][1] = schedule2[6][1];
                    System.out.println("Great!");
                }
                default -> System.out.println("Sorry, I don't understand you, please try again.");
            }
        }
    }

    public static String[][] createAPlanner() {
        String[][] schedule = new String[7][2];
        schedule[0][0] = "Monday";
        schedule[0][1] = "go to the post office, finish reading the book";
        schedule[1][0] = "Tuesday";
        schedule[1][1] = "pay the bills";
        schedule[2][0] = "Wednesday";
        schedule[2][1] = "visit the gym";
        schedule[3][0] = "Thursday";
        schedule[3][1] = "nothing";
        schedule[4][0] = "Friday";
        schedule[4][1] = "to meet friends";
        schedule[5][0] = "Saturday";
        schedule[5][1] = "help grandma";
        schedule[6][0] = "Sunday";
        schedule[6][1] = "watch movie";

        return schedule;
    }
}
