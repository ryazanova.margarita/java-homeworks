package homework1;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class GuessTheNumber {
    public static void main(String[] args) {

        Random rand = new Random();
        int rand_number = rand.nextInt(101);
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter your name: ");
        String name = scan.nextLine();
        System.out.println("Let the game begin");
        System.out.print("Enter the number: ");

        int[] my_answer = new int[0];


        while (true) {
            int[] my_answer_copy = new int[my_answer.length + 1];
            System.arraycopy(my_answer, 0, my_answer_copy, 0, my_answer.length);


            if (!scan.hasNextInt()) {
                System.err.println("It's not a number");
                scan.next();
                continue;
            }
            int number = scan.nextInt();
            my_answer_copy[my_answer_copy.length - 1] = number;
            my_answer = my_answer_copy;
            if (number > rand_number) {
                System.out.println("Your number is too big. Please, try again.");
            } else if (number < rand_number) {
                System.out.println("Your number is too small. Please, try again.");
            }
            if (number == rand_number) {
                System.out.printf("Congratulations, %s!\n", name);

                Arrays.sort(my_answer);
                System.out.println("Your numbers:" + Arrays.toString(my_answer));
                break;
            }

        }
    }
}


