package homework.happyfamily;


import java.util.*;

public class Main {
    public static void main(String[] args) {
        Family f1 = new Family();
        Family f2 = new Family();
        Family f3 = new Family();
        Family f4 = new Family();

        DomesticCat cat1 = new DomesticCat( "Lilu",f1);
        cat1.eat();
        cat1.respond();
        Human h1 = new Human("Daria", "Konovalenko", 21,f1);
        h1.greetPet(cat1);
        h1.describePet(cat1);

        Map<String, String> schedule1 = new HashMap<>();
        schedule1.put(DayOfWeek.MONDAY.name(), "go to the gym");
        schedule1.put(DayOfWeek.SATURDAY.name(), "go for a run");
        Human h2 = new Human("John", "Brown", 46, 87, schedule1,f2);
        System.out.println(h2);

        Set<String> habits1 = new HashSet<>();
        habits1.add("bark");
        habits1.add("play with the ball");
        Dog dog1 = new Dog( "Jake", 7, 34, habits1,f3);
        System.out.println(dog1);
        Human h3 = new Human("Inga", "Kotova", 32,f3);
        h3.feedPet(false, dog1);
       Human h4 = new Human("Denis", "Kotov", 35, f3);
       Human h5 = new Human("Dmitriy", "Kotov", 13,f3);
       Human h6 = new Human("Karina", "Kotova", 4,f3);
        f3 = new Family(h3, h4, new ArrayList<Human>());
        f3.addChild(h5);
        f3.addChild(h6);
        f3.countFamily();
        System.out.println(f3);
        f3.deleteChild2(h5);
        f3.countFamily();
        System.out.println(f3);

        Set<String> habits2 = new HashSet<>();
        habits2.add("bark");
        habits2.add("play with the ball");
       Dog dog2= new Dog( "Jake", 7, 78, habits2,f4);
        dog2.foul();
        System.out.println(dog2);
        Human h7 = new Human("Michael", "Morgan", 23, f4);
        h7.feedPet(true, dog2);
        Human h8 = new Human("Emma", "Smith", 23, f4);
        Human h9 = new Human("Rob", "Morgan", 2,f4);
        f4 = new Family(h8, h7, new ArrayList<Human>(), dog2);
        f4.addChild(h9);
        System.out.println(f4);


//        int maxObjects = 10000000;
//
//        for (int i = 0; i < maxObjects; i++) {
//            Human human = new Human();
//            if (i % 10000 == 0) {
//                System.gc();
//            }
//        }

    }
}
