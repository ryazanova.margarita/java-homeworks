package homework.happyfamily;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Family {
    static {
        System.out.println("|| Launch of the Family class ||");
    }

    {
        System.out.println("| Family:new family instance |");
    }

    private Human mother;
    private Human father;
    private List<Human> children;
    private Pet pet;

    public boolean addChild(Human child) {
        boolean isAdded = false;
        child.setFamily(this);
        if (children == null) {
            children = new ArrayList<>();
        }
        children.add(child);
        isAdded = true;

        return isAdded;
    }

    public boolean deleteChild1(int index) {
        boolean isDeleted = false;

        if (children != null && index >= 0 && index < children.size()) {
            children.remove(index);
            isDeleted = true;
        }

        return isDeleted;
    }

    public boolean deleteChild2(Human child) {
        boolean isDeleted = false;

        if (children != null) {
            isDeleted = children.remove(child);
        }

        return isDeleted;
    }

    public int countFamily() {
        int quantity = 2 + (children != null ? children.size() : 0);
        return quantity;
    }

    @Override
    public String toString() {
        return "Family{mother=%s, father=%s, children=%s, pet=%s}".formatted(mother, father, children, pet);
    }

    public Family() {
    }

    public Family(Human mother, Human father, List children, Pet pet) {
        this.mother = mother;
        this.father = father;
        this.children = children;
        this.pet = pet;
    }

    public Family(Human mother, Human father, List children) {
        this.mother = mother;
        this.father = father;
        this.children = children;
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public List<Human> getChildren() {
        return children;
    }

    public void setChildren(List<Human> children) {
        this.children = children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    @Override
    public void finalize(){
        System.out.println("deleting : ");
        System.out.println(this);
    }

}




