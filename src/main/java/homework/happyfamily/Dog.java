package homework.happyfamily;

import java.util.Set;

public class Dog extends Pet implements Pet.Foul{
    private final Species species = Species.DOG;
    @Override
    public void respond(){
        System.out.println("Woof Woof");
    }

    @Override
    public void foul(){System.out.println("Need to cover your tracks well...");}

    @Override
    public Species getSpecies() {
        return species;
    }

    public Dog() {
    }

    public Dog(String nickname, int age, int trickLevel, Set habits, Family family) {
        super(nickname, age, trickLevel, habits, family);
    }

    public Dog(String nickname, Family family) {
        super(nickname, family);
    }
}


