package homework.happyfamily;

import java.util.Set;

public class DomesticCat extends Pet implements Pet.Foul{
    private final Species species = Species.DOMESTIC_CAT;
    @Override
    public void respond(){
        System.out.println("Meow");
    }

    @Override
    public void foul(){System.out.println("This jar has no place on the table here");}

    @Override
    public Species getSpecies() {
        return species;
    }
    public DomesticCat() {
    }

    public DomesticCat(String nickname, int age, int trickLevel, Set habits, Family family) {
        super(nickname, age, trickLevel, habits, family);
    }

    public DomesticCat(String nickname, Family family) {
        super(nickname, family);
    }
}

