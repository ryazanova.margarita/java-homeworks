package homework.happyfamily;

import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.Random;

public class Human {
    static {
        System.out.println("|| Launch of the Human class ||");
    }

    {
        System.out.println("| Human:new human instance |");
    }

    private String name;
    private String surname;
    private int year;
    private int iq;//[0;100]
    private Map schedule;
    private Family family;

    public void greetPet(Pet p) { System.out.printf("Hi, %s, didn't you miss me?\n", family.getPet().getNickname());

    }

    public void describePet(Pet p) {
        System.out.printf("I have a %s, she is %d years old, she", family.getPet().getSpecies(), family.getPet().getAge());
        if (family.getPet().getTrickLevel() > 50) {
            System.out.println("very cunning.");
        } else if (family.getPet().getTrickLevel() <= 50) {
            System.out.println("almost not cunning.");
        } else {
            System.err.println("ERROR!\n");
        }
    }

    public boolean feedPet(boolean itsTimeToFeed, Pet p) {
        if (itsTimeToFeed) {
            System.out.printf("Feed %s\n", family.getPet().getNickname());
            return true;
        } else {
            Random random = new Random();
            int rand_number = random.nextInt(101);

            if (rand_number < family.getPet().getTrickLevel()) {
                System.out.printf("Feed %s\n", family.getPet().getNickname());
                return true;
            } else if (rand_number >= family.getPet().getTrickLevel()) {
                System.out.printf("I don't think %s is hungry\n", family.getPet().getNickname());
                return false;
            } else if (family.getPet().getTrickLevel() < 0 || family.getPet().getTrickLevel() > 100) {
                System.err.println("ERROR!\n");
                return false;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return "Human{name='%s', surname='%s', year=%d, iq=%d, schedule=%s}".formatted(name, surname, year, iq, schedule);
    }

    public Human(String name, String surname, int year, Family family) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.family=family;

    }

    public Human(String name, String surname, int year, int iq, Map schedule, Family family) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
        this.family=family;
    }

    public Human() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public Map getSchedule() {
        return schedule;
    }

    public void setSchedule(Map schedule) {
        this.schedule = schedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return year == human.year && Objects.equals(name, human.name) && Objects.equals(surname, human.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, year);
    }

    @Override
    public void finalize(){
        System.out.println("deleting : ");
        System.out.println(this);
    }

}
