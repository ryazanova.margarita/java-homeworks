package homework.happyfamily;

import java.util.Set;

public class RoboCat extends Pet implements Pet.Foul{
    private final Species species = Species.ROBOCAT;
    @Override
    public void respond(){
            System.out.printf("Hello, owner! I %s. I missed\n", getNickname());
    }

    @Override
    public void foul(){System.out.println("Owner, i made a mess.");}


    @Override
    public Species getSpecies() {
        return species;
    }
    public RoboCat() {
    }

    public RoboCat(String nickname, int age, int trickLevel, Set habits, Family family) {
        super(nickname, age, trickLevel, habits, family);
    }

    public RoboCat(String nickname, Family family) {
        super(nickname, family);
    }
}

