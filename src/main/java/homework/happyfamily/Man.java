package homework.happyfamily;

import java.util.Map;

public final class Man extends Human{
    public Man(String name, String surname, int year, int iq, Map schedule, Family family) {
        super(name, surname, year, iq, schedule, family);
    }

    public void repairCar(){
        System.out.println("I'll go check what's wrong with my car");
    }
}
