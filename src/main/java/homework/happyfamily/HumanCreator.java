package homework.happyfamily;

public interface HumanCreator {
    Human bornChild();
}
