package homework.happyfamily;

import java.util.Map;
import java.util.Random;

public final class Woman extends Human implements HumanCreator {
    private Random random=new Random();

    private String[] femaleNames={"Daria","Inga", "Karina","Emma","Barbara","Sara",
            "Tamara","Oksana","Katerina","Anastasia","Karolina","Olga","Margarita"};
    private String[] maleNames={"Taras","Mark", "Jim","Vladislav","Alex","Rob",
            "Michael","Denys","Dmitriy","John","Nikita","Andy","Tomas","Ben"};

    public Woman(String name, String surname, int year, int iq, Map schedule, Family family) {
        super(name, surname, year, iq, schedule, family);
    }
    @Override
    public Human bornChild(){
        boolean isWoman=random.nextBoolean();
        int motherIq=this.getIq();
        int fatherIq=this.getFamily().getFather().getIq();
        int iq=(motherIq+fatherIq)/2;
        String name;
        String surname = this.getFamily().getFather().getSurname();
        Family family= this.getFamily();

        if(isWoman){
            name=femaleNames[random.nextInt(femaleNames.length)];
            return new Woman(name,surname,0,iq,null,family);
        }else {
            name=maleNames[random.nextInt(maleNames.length)];
            return new Man(name,surname,0,iq,null,family);
        }
    }

    public void makeup(){
        System.out.println("It wouldn't hurt to do some makeup");
    }
}
