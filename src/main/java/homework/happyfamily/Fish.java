package homework.happyfamily;

import java.util.Set;

public class Fish extends Pet{
    private final Species species = Species.FISH;
    @Override
    public void respond(){
        System.out.println("Bloop Bloop");
    }
    @Override
    public Species getSpecies() {
        return species;
    }


    public Fish() {
    }

    public Fish(String nickname, int age, int trickLevel, Set habits, Family family) {
        super(nickname, age, trickLevel, habits, family);
    }

    public Fish(String nickname, Family family) {
        super(nickname, family);
    }
}
