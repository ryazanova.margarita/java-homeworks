package homework.happyfamily;


import java.util.Arrays;
import java.util.Objects;
import java.util.Set;

public abstract class Pet {
    static {
        System.out.println("|| Launch of the Pet class ||");
    }

    {
        System.out.println("| Pet:new pet instance |");
    }

    private final Species species = Species.UNKNOWN;
    private String nickname;
    private int age;
    private int trickLevel;//[0;100]
    private Set habits;
    private Family family;

    public void eat() {
        System.out.println("I eat!");
    }

    public abstract void respond();

    public interface Foul {
        void foul();
    }

    @Override
    public String toString() {
        return String.format("%s{nickname='%s', age=%d, trickLevel=%d, habits=%s}",
                getSpecies(), nickname, age, trickLevel, habits);
    }


    public Pet() {

    }

    public Pet(String nickname, int age, int trickLevel, Set habits, Family family) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
        this.family = family;
        family.setPet(this);
    }

    public Pet(String nickname, Family family) {
        this.nickname = nickname;
        this.family = family;
        family.setPet(this);
    }

    public abstract Species getSpecies();

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public Set getHabits() {
        return habits;
    }

    public void setHabits(Set habits) {
        this.habits = habits;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return age == pet.age && Objects.equals(nickname, pet.nickname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nickname, age);
    }

    @Override
    public void finalize() {
        System.out.println("deleting : ");
        System.out.println(this);
    }

}


