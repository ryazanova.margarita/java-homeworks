package homework.happyfamily;

public enum Species {
    DOG(false, 4, true),
    DOMESTIC_CAT(false, 4, true),
    FISH(false, 0, false),
    ROBOCAT(false,4,true),

    UNKNOWN();

    boolean canFly;
    int numberOfLegs;
    boolean hasFur;

    Species(boolean canFly, int numberOfLegs, boolean hasFur) {
        this.canFly = canFly;
        this.numberOfLegs = numberOfLegs;
        this.hasFur = hasFur;
    }

    Species() {
    }
    public static Species fromString(String name) {
        try {
            return valueOf(name);
        } catch (IllegalArgumentException e) {
            return UNKNOWN;
        }
    }

    @Override
    public String toString() {
        return "%s{canFly=%s, numberOfLegs=%d, hasFur=%s}".formatted(name(), canFly, numberOfLegs, hasFur);

    }


}


