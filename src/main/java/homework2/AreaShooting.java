package homework2;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class AreaShooting {
    final static int FIELD_SIZE = 5;

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Random rand = new Random();

        int x = 0;
        int y = 0;

        int x_target = rand.nextInt(FIELD_SIZE - 1) + 1;
        int y_target = rand.nextInt(FIELD_SIZE - 1) + 1;

        int[][] field = new int[FIELD_SIZE][FIELD_SIZE];

        System.out.println("All Set. Get ready to rumble!");
        System.out.println("You need to enter a number from 1 to 5 and guess where the target is.");

        while (true) {
            for (int i = 0; i < field.length + 1; i++) {
                for (int j = 0; j < field.length + 1; j++) {
                    if (j == 0) System.out.print(i + "|");
                    else if (i == 0 && j > 0) System.out.print(j + "|");
                    else {
                        switch (field[i - 1][j - 1]) {
                            case 0: {
                                System.out.print("-" + "|");
                                break;
                            }
                            case 1: {
                                System.out.print("*" + "|");
                                break;
                            }
                            case 2: {
                                System.out.print("X" + "|");
                                break;
                            }
                            default:
                                ;
                        }
                    }
                    ;
                }
                System.out.println();
            }

            System.out.print("Enter x: ");
            while (true) {
                if (!scan.hasNextInt()) {
                    System.err.println("This is not a number");
                    System.out.print("Try again.\nEnter x: ");
                    scan.next();
                } else {
                    x = scan.nextInt();
                    break;
                }
            }
            while (x <= 0 || x > FIELD_SIZE) {
                System.err.println("You are out of bounds");
                System.out.print("Try again.\nEnter x: ");
                if (!scan.hasNextInt()) {
                    System.err.println("This is not a number");
                    System.out.print("Try again.\nEnter x: ");
                    scan.next();
                } else {
                    x = scan.nextInt();
                }
            }

            System.out.print("Enter y: ");
            while (true) {
                if (!scan.hasNextInt()) {
                    System.err.println("This is not a number");
                    System.out.print("Try again.\nEnter y: ");
                    scan.next();
                } else {
                    y = scan.nextInt();
                    break;
                }
            }
            while (y <= 0 || y > FIELD_SIZE) {
                System.err.println("You are out of bounds");
                System.out.print("Try again.\nEnter y: ");
                if (!scan.hasNextInt()) {
                    System.err.println("This is not a number");
                    System.out.print("Try again.\nEnter y: ");
                    scan.next();
                } else {
                    y = scan.nextInt();
                }
            }

            if (x == x_target && y == y_target) {
                field[x-1][y-1] = 2;
                System.out.println("You won!");
            } else if (x != x_target && y != y_target){
                field[x - 1][y - 1] = 1;
            }
        }
    }
}



