import homework.happyfamily.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class BirthTest {
    private Family family;
    private Woman woman;
    private Man man;
    @BeforeEach
    public void setUp(){
        family=new Family(null,null,null);
        man=new Man("Andy","West",24,87,null,family);
        woman=new Woman("Emma","Brown",22,32,null,family);
        family.setFather(man);
        family.setMother(woman);
    }

    @Test
    public void testBirth(){
        Human child=woman.bornChild();
        System.out.println(child);
    }
}
