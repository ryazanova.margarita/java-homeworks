import homework.happyfamily.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class EqualsHumanTest {
    Family family;
    Human h1 = new Human("Tamara", "Klipenko", 1995, null);
    Human h2 = new Human("Tamara", "Klipenko", 1995, family);
    Human h3 = new Human("Oksana","Morozova",2004,null);
    Human h4 = new Human("Oksana","Morozova",2022,null);
    Human h5= new Human("Nikita","Kolesnyk",2009,null);
    Human h6 = new Human("Nikita","Vovk",2009,null);
    Human h7 = new Human("Katerina","Konoval",2001,null);
    Human h8 = new Human("Anastasia","Konoval",2001,null);

    Human h9 = new Human("Tamara", "Kroleva", 1958, null);
    Human h10 = new Human("Tamara", "Lipenko", 1993, null);
    Human h11 = new Human("Karolina","Morozova",2011,null);
    Human h12 = new Human("Olga","Morozova",2015,null);
    Human h13= new Human("Alex","John",2009,null);
    Human h14 = new Human("Mark","Baker",2009,null);
    Human h15 = new Human("Margarita","Nosova",2000,null);
    Human h16 = new Human("Anastasia","Kotova",2005,null);

    @Test
    public void testEqualsHuman_CompleteCoincidence() {
        boolean compare = h1.equals(h2);
        assertEquals(true, compare);
    }

    @Test
    public void testEqualsHuman_MathchingFirstNameAndLastName() {
        boolean compare = h3.equals(h4);
        assertEquals(false, compare);
    }

    @Test
    public void testEqualsHuman_NameAndAgeMatch() {
        boolean compare = h5.equals(h6);
        assertEquals(false, compare);    }

    @Test
    public void testEqualsHuman_MatchOfSurnameAndAge() {
        boolean compare = h7.equals(h8);
        assertEquals(false, compare);
    }
    @Test
    public void testEqualsHuman_NameMatch() {
        boolean compare = h9.equals(h10);
        assertEquals(false, compare);
    }
    @Test
    public void testEqualsHuman_LastNameCoincidence() {
        boolean compare = h11.equals(h12);
        assertEquals(false, compare);
    }
    @Test
    public void testEqualsHuman_CoincidenceAge() {
        boolean compare = h13.equals(h14);
        assertEquals(false, compare);
    }
    @Test
    public void testEqualsHuman_CompleteMismatch() {
        boolean compare = h15.equals(h16);
        assertEquals(false, compare);
    }
}
