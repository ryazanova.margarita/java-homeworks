import homework.happyfamily.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
public class HashCodeHumanTest {
    Family family;
    Human h1 = new Human("Tamara", "Klipenko", 1995, null);
    Human h2 = new Human("Tamara", "Klipenko", 1995, family);

    @Test
    public void testHashCodeHuman(){
        int hashCode1= h1.hashCode();
        int hashCode2 = h2.hashCode();
        System.out.println(hashCode1);
        System.out.println();
        System.out.println(hashCode2);

        assertEquals(hashCode1,hashCode2);
    }
}
