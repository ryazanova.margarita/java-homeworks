import homework.happyfamily.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AddingAChildTest {
    private Family family;
    Human mother = new Human("Barbara", "John", 38, family);
    Human father = new Human("Andy", "John", 40, family);
    Human h1 = new Human("Sara", "John", 16, family);
    Human h2 = new Human("Tomas", "John", 11, family);
    Human h3 = new Human("Ben", "John", 5, family);

    @BeforeEach
    public void setUp1() {
        List<Human> children = new ArrayList<>();
        children.add(h1);
        children.add(h2);
        family = new Family(mother, father, children);

    }

    @Test
    public void testAddingAChild() {
        List<Human> children2 = family.getChildren();

        boolean addchild=family.addChild(h3);

        assertEquals(true, addchild);
    }
}