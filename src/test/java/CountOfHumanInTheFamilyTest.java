import homework.happyfamily.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class CountOfHumanInTheFamilyTest {
    private Family family;
    Human mother = new Human("Barbara", "John", 38, family);
    Human father = new Human("Andy", "John", 40, family);
    Human h1 = new Human("Sara", "John", 16, family);
    Human h2 = new Human("Tomas", "John", 11, family);

    @BeforeEach
    public void setUp1() {
        family = new Family(mother, father, new ArrayList<Human>());
        family.addChild(h1);
        family.addChild(h2);

    }

    @Test
    public void testCountOfHumanInTheFamily() {
        int quantity=family.countFamily();
        System.out.println(quantity);
    }
}
