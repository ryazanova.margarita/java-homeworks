import homework.happyfamily.*;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


public class PetToStringTest {
    @Test
    public void testToString_Positive() {
        Family family = new Family();
        Set<String> habits = new HashSet<>();
        habits.add("Bark");
        Dog dog = new Dog("Jake", 10, 80, habits, family);
        String expectedString = "DOG{canFly=false, numberOfLegs=4, hasFur=true}{nickname='Jake', age=10, trickLevel=80, habits=[Bark]}";

        assertEquals(expectedString, dog.toString());
    }

    @Test
    public void testToString_Negative() {
        Dog dog = new Dog();

        assertNotNull(dog.toString());
    }
}
