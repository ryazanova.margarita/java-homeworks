import homework.happyfamily.*;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


public class HumanToStringTest {
    @Test
    public void testToString_Positive() {
        Family family = new Family();
        Map<String, String> schedule = new HashMap<>();
        schedule.put(DayOfWeek.WEDNESDAY.name(), "watch movie");
        schedule.put(DayOfWeek.FRIDAY.name(), "help grandma");
        Human human = new Human("Mark", "Baker", 28, 57, schedule, family);
        String expectedString = "Human{name='Mark', surname='Baker', year=28, iq=57, schedule={WEDNESDAY=watch movie, FRIDAY=help grandma}}";

        assertEquals(expectedString, human.toString());
    }

    @Test
    public void testToString_Negative() {
        Human human = new Human();

        assertNotNull(human.toString());
    }
}
