import homework.happyfamily.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class DeletingAChildByIndexTest {
    private Family family;

    @BeforeEach
    public void setUp1() {
        Human mother = new Human("Barbara", "John", 38, family);
        Human father = new Human("Andy", "John", 40, family);
        Human h1 = new Human("Sara", "John", 16, family);
        Human h2 = new Human("Tomas", "John", 11, family);
        Human h3 = new Human("Ben", "John", 5, family);
        family = new Family(mother, father, new ArrayList<Human>());
        family.addChild(h1);
        family.addChild(h2);
        family.addChild(h3);
    }

    @Test
    public void testDeletingAChildByIndex_Positive() {
        List<Human> children3 = family.getChildren();

        boolean deletechild = family.deleteChild1(1);

        assertEquals(true, deletechild);

    }

    @Test
    public void testDeletingAChildByIndex_Negative() {
        List<Human> children3 = family.getChildren();
        family.deleteChild1(4);

        assertEquals(false, family.deleteChild1(4));

    }
}
