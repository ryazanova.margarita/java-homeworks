import homework.happyfamily.*;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


public class FamilyToStringTest {

    @Test
    public void testToString_Positive() {
        Family family = new Family();

        Map<String, String> schedule1 = new LinkedHashMap<>();
        schedule1.put(DayOfWeek.TUESDAY.name(), "nothing");
        schedule1.put(DayOfWeek.THURSDAY.name(), "pay the bills");
        Human human1 = new Human("Emma", "Smith", 35, 67, schedule1,family);

        Map<String, String> schedule2 = new HashMap<>();
        schedule2.put(DayOfWeek.WEDNESDAY.name(), "watch movie");
        schedule2.put(DayOfWeek.FRIDAY.name(), "help grandma");
        Human human2 = new Human("Michael", "Morgan", 35,68, schedule2, family);

        Map<String, String> schedule3 = new HashMap<>();
        schedule3.put(DayOfWeek.MONDAY.name(), "go to the gym");
        schedule3.put(DayOfWeek.SATURDAY.name(), "go for a run");
        Human human3 = new Human("Rob", "Morgan", 16,56,schedule3,family);

        Set<String> habits = new HashSet<>();
        habits.add("bark");
        habits.add("play with the ball");
        Dog dog = new Dog("Jake", 7, 78, habits, family);

        family = new Family(human1, human2, new ArrayList<Human>(), dog);
        family.addChild(human3);

        String expectedString = "Family{mother=Human{name='Emma', surname='Smith', year=35, iq=67, schedule={TUESDAY=nothing, THURSDAY=pay the bills}}, father=Human{name='Michael', surname='Morgan', year=35, iq=68, schedule={WEDNESDAY=watch movie, FRIDAY=help grandma}}, children=[Human{name='Rob', surname='Morgan', year=16, iq=56, schedule={MONDAY=go to the gym, SATURDAY=go for a run}}], pet=DOG{canFly=false, numberOfLegs=4, hasFur=true}{nickname='Jake', age=7, trickLevel=78, habits=[bark, play with the ball]}}";
        assertEquals(expectedString, family.toString());
    }

    @Test
    public void testToString_Negative() {
        Family family= new Family();

        assertNotNull(family.toString());
    }
}
